<!-- PHP HERE -->

<!-- Vladimir php start -->
<?php
$img1='https://images.rolex.com/2019/catalogue/images/upright-bba-with-shadow/m126710blnr-0002.png';
$name1='Rolex';
$price1=8500;
$shipping1=100;
$rating1=4.7;
$discount1=5;
$gender1='Male';
$product1='In Stock';

$price1_new=$price1 - ($price1 * ($discount1 / 100));

$totalprice1=$price1 - ($price1 * ($discount1 / 100)) + $shipping1;

if($gender1=='Male'){
  $bgcolor1='#f7f7f7';
} else{
  $bgcolor1='#ffe6ee';
}

if($product1=='In Stock'){
    $product1_color='text-success';
} else{
  $product1_color='text-danger';
}
?>
<!-- Vladimir php end -->


<!-- Daniela php start -->
<?php
$img2='https://slimages.macys.com/is/image/MCY/products/4/optimized/3249654_fpx.tif?op_sharpen=1&wid=500&hei=613&fit=fit,1&$filtersm$';
$name2='Baume';
$price2=422.99;
$shipping2=29.99;
$rating2=4.3;
$discount2=0;
$gender2='Female';
$product2='In Stock';

$price2_new=$price2 - ($price2 * ($discount2 / 100));

$totalprice2=$price2 - ($price2 * ($discount2 / 100)) + $shipping2;

if($gender2=='Male'){
  $bgcolor2='#f7f7f7';
} else{
  $bgcolor2='#ffe6ee';
}

if($product2=='In Stock'){
    $product2_color='text-success';
} else{
  $product2_color='text-danger';
}
?>

<!-- Daniela php end -->


<!-- Darko php start -->
<?php
$img3='https://assets.myntassets.com/dpr_1.5,q_60,w_400,c_limit,fl_progressive/assets/images/10052451/2019/10/23/136def77-6f7e-429a-aa73-6519619190e71571832020111-Titan-Men-Watches-6661571832018840-1.jpg';
$name3='Titan';
$price3=349.99;
$shipping3=20;
$rating3=5;
$discount3=10;
$gender3='Male';
$product3='Out of stock';
$pricedsc3=$price3;
$pricettl3=0;
$titledsc3="";

$price3_new=$price3 - ($price3 * ($discount3 / 100));

$totalprice3=$price3 - ($price3 * ($discount3 / 100)) + $shipping3;

if ($discount3>0){
  $titledsc3="$discount3 % OFF!";
  $pricedsc3=$price3/100*(100-$discount3);
  $pricettl3=$pricedsc3+$shipping3;
} else {$pricettl3=$price3+$shipping3;};

if ($product3=="In Stock"){
    $stockclass="text-success";
} elseif ($product3=="Out of Stock") { 
  $stockclass="text-danger";
};

if($gender3=='Male'){
  $bgcolor3='#f7f7f7';
} elseif ($gender3=='Female'){
  $bgcolor3='#ffe6ee';
}
?>
<!-- Darko php end -->


<!-- Nade php start -->
<?php
$img4='https://hips.hearstapps.com/vader-prod.s3.amazonaws.com/1599074228-ac33b889-8e4b-4ab1-9e4a-90447d621aac-1599074219.jpg?crop=1xw:1xh;center,top&resize=480:*';
$name4='Quartz';
$price4=199.99;
$shipping4=7.99;
$rating4=5;
$discount4=20;
$gender4='Female';
$product4='In Stock';
$pricedsc4=$price4;
$pricettl4=0;
$stockclass=1;
$titledsc="";

if ($discount4>0){
  $titledsc="$discount4 % OFF!";
  $pricedsc4=$price4/100*(100-$discount4);
  $pricettl4=$pricedsc4+$shipping4;
} else {$pricettl4=$price4+$shipping4;};

if ($product4=="In Stock"){
    $stockclass="text-success";
} elseif ($product4=="Out of Stock") { 
  $stockclass="text-danger";
};

if($gender4=='Male'){
  $bgcolor4='#f7f7f7';
} elseif ($gender4=='Female'){
  $bgcolor4='#ffe6ee';
}
?>
<!-- Nade php end -->


<!-- Stefani php start -->
<?php 
    
  $name5="Lauriel";
  $img5='https://images-na.ssl-images-amazon.com/images/I/61o4-xd5UbL._UL1200_.jpg';
  $price5=219.99;
  $shipping5=9.99;
  $rating5=2.3;
  $discount5=30;
  $gender5='Male';
  $product5='Out of Stock';
  $pricedsc5=$price5;
  $pricettl5=0;
  


  if ($discount5>0){
    $pricedsc5=$price5/100*(100-$discount5);
    $pricettl5=$pricedsc5+$shipping5;
  } else {$pricettl5=$price5+$shipping5;};
  
  if ($product5=="In Stock"){
      $stockclass="text-success";
  } elseif ($product5=="Out of Stock") { 
    $stockclass="text-danger";
  };
  
  if($gender5=='Male'){
    $bgcolor5='#f7f7f7';
  } elseif ($gender5=='Female'){
    $bgcolor5='#ffe6ee';
  }
?>
<!-- Stefani php end -->


<!-- Filip php start -->
<?php
    $image6 = "https://catwatches.com/pub/media/catalog/product/cache/8c0bf43db71c47b7671bc668a7b6c127/s/f/sf.161.63.313-min.png";
    $gender6 = 'male';
    $price6 = 500;
    $discountPercentage6 = 0;
    $shipping6 = 35;
    $rating6 = ' 5';
    $stock6 = FALSE;

    // body bg_color
    if ($gender6 == 'male') {
      $bgColor6 = '#f7f7f7';
      } else {
      $bgColor6 = '#ffe6ee';
    }

    
    if ($discountPercentage6 > 0) {
      $discountIndicator6 = "$discountPercentage6% OFF!";  // Title changer     
      $discountStyle6 = 'color: grey; text-decoration: line-through;';  // Makes the price grey and line-through      
      $sellingPrice6 = ($price6 - ($price6 * ($discountPercentage6 / 100)));  // Discount calculator      
      $sellingPriceText6 = "US $sellingPrice6$"; // Price with discount calculated      
      $total6 = $sellingPrice6 + $shipping6; // Total calculator
    } else {
      $discountIndicator6 = "";
      $sellingPriceText6 = "";
      $total6 = $price6 + $shipping6;
    }
   
    // In stock or not
    if ($stock6 == TRUE) {
      $stockTextColor6 = "text-success";
      $stockText6 = "In stock";
      $btnColor6 = "btn-success";
      $btnText6 = "Order";
    } else {
      $stockTextColor6 = "text-danger";
      $stockText6 = "Out of stock";
      $btnColor6 = "btn-danger";
      $btnText6 = "N/A";
    }
    
  ?>
<!-- Filip php end -->


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <title>Hello, world!</title>
    <style>
    /* Vladimir css start */
    .card1-bg{
        background-color:<?php echo $bgcolor1; ?>;
      }
     /* Vladimir css end */

      /* Daniela css start */
      .card2-bg{
        background-color:<?php echo $bgcolor2; ?>;
      }
      /* Daniela css end */

      /* Darko css start */
      .card3-bg{
        background-color:<?php echo $bgcolor3; ?>;
      }
      /* Darko css end */
      .card4-bg{
        background-color:<?php echo $bgcolor4; ?>;
      }

      /* Stefani css start */
      .card5-bg{
        background-color:<?php echo $bgcolor5; ?>;
      }
      /* Stefani css end */
    </style>
  </head>
  <body>
    

    <div class="container">
      <h1 class="my-4 ml-3">Products</h1>
    <div class="card-group">

      <!-- Vladimir card start -->

      <div class="card m-3 border rounded card1-bg">
        <img src="<?php echo $img1;?>" class="card-img-top img1" alt="Rolex watch">
        <div class="card-body p-4">
          <?php if($discount1>0) { ?> 
            <h3 class="card-title"><?php echo "$name1"." "."$discount1"."% OFF";?></h3>
          <?php } else { ?>
            <h3 class="card-title"><?php echo $name1;?></h3>
          <?php } ?>
          <p class="card-text">This is a watch. A brand new Watch from <?php echo "$name1";?>. Waterproof, stainless steel</p> 
          <?php if($discount1>0) { ?> 
          <p class="h5 font-weight-bold"> <del class="text-secondary">US &dollar;<?php echo $price1 ?></del> US &dollar;<?php echo $price1_new;?></p>
          <?php } else { ?>
            <p class="h5 font-weight-bold">US &dollar;<?php echo $price1; ?></p>
          <?php } ?>          
          <p>Shipping : &dollar;<?php echo "$shipping1";?></p>
          <p>Rating : <?php echo "$rating1";?></p>
          <p class="<?php echo $product1_color; ?>"><?php echo $product1; ?></p>
          <p class="font-weight-bold">Total : &dollar;<?php echo $totalprice1;?></p>
          <?php if($product1=='In Stock') { ?> 
          <button type="button" class="btn btn-block btn-success"><p class="h5">Order</p></button>
          <?php } else { ?>
            <button type="button" class="btn btn-block btn-danger"><p class="h5">N/A</p></button>
          <?php } ?>  
        </div>
      </div>

      <!-- Vladimir card end -->


      <!-- Daniela card start -->

      <div class="card m-3 border rounded card2-bg">
        <img src="<?php echo $img2;?>" class="card-img-top" alt="Baume watch">
        <div class="card-body p-4">
          <?php if($discount2>0) { ?> 
            <h3 class="card-title"><?php echo "$name2"." "."$discount2"."% OFF";?></h3>
          <?php } else { ?>
            <h3 class="card-title"><?php echo $name2;?></h3>
          <?php } ?>
          <p class="card-text">This is a watch. A brand new Watch from <?php echo "$name2";?>. Waterproof, stainless steel</p> 
          <?php if($discount2>0) { ?> 
          <p class="h5 font-weight-bold"> <del class="text-secondary">US &dollar;<?php echo $price2 ?></del> US &dollar;<?php echo $price2_new;?></p>
          <?php } else { ?>
            <p class="h5 font-weight-bold">US &dollar;<?php echo $price2; ?></p>
          <?php } ?>          
          <p>Shipping : &dollar;<?php echo "$shipping2";?></p>
          <p>Rating : <?php echo "$rating2";?></p>
          <p class="<?php echo $product2_color; ?>"><?php echo $product2; ?></p>
          <p class="font-weight-bold">Total : &dollar;<?php echo $totalprice2;?></p>
          <?php if($product2=='In Stock') { ?> 
          <button type="button" class="btn btn-block btn-success"><p class="h5">Order</p></button>
          <?php } else { ?>
            <button type="button" class="btn btn-block btn-danger"><p class="h5">N/A</p></button>
          <?php } ?>  
        </div>
      </div>

      <!-- Daniela card end -->


      <!-- Darko card start -->

      <div class="card m-3 border rounded card3-bg">
      <img src="<?php echo $img3;?>" class="card-img-top" alt="...">
        <div class="card-body p-4">
        <?php if($discount3>0) { ?> 
            <h3 class="card-title"><?php echo "$name3"." "."$discount3"."% OFF";?></h3>
          <?php } else { ?>
            <h3 class="card-title"><?php echo $name3;?></h3>
          <?php } ?>
          <p class="card-text">This is a watch. A brand new Watch from <?php echo "$name3";?>. Waterproof, stainless steel.</p>
          <p class="h5 py-1"> <?php if ($discount3>0){ ?> <del class="text-secondary">US &dollar;<?php echo "$price3" ?> </del> <?php } ?> US &dollar;<?php echo number_format($pricedsc3,2); ?></p>
          <p class="py-1">Shipping: &dollar;<?php echo "$shipping3" ?></p>
          <p class="py-1">Rating: <?php echo "$rating3" ?></p>
          <p class="py-1 <?php echo "$stockclass" ?>"><?php echo "$product3" ?></p>
          <p class="py-2 h5 font-weight-bold">Total: &dollar;<?php echo number_format($pricettl3,2) ?></p>
          <?php if ($product3=="In Stock"){ ?>
            <button type="button" class="btn btn-success w-100"><p class="h5">Order</p></button>
            <?php } else { ?>
              <button type="button" class="btn btn-danger w-100"><p class="h5">N/A</p></button>
          <?php } ?>
        </div>
      </div>
      
      <!-- Darko card end -->

    </div>

    <div class="card-group">

      <!-- Nade card start -->

      <div class="card m-3 border rounded card4-bg">
      <img src="<?php echo $img4;?>" class="card-img-top" alt="...">
        <div class="card-body p-4">
          <h3 class="card-title"><?php echo "$name4"." "."$titledsc"; ?></h3>
          <p class="card-text">This is a watch. A brand new Watch from <?php echo "$name4";?>. Waterproof, stainless steel.</p>
          <p class="h5 py-1"> <?php if ($discount4>0){ ?> <del class="text-secondary">US &dollar;<?php echo "$price4" ?> </del> <?php } ?> US &dollar;<?php echo number_format($pricedsc4,2); ?></p>
          <p class="py-1">Shipping: &dollar;<?php echo "$shipping4" ?></p>
          <p class="py-1">Rating: <?php echo "$rating4" ?></p>
          <p class="py-1 <?php echo "$stockclass" ?>"><?php echo "$product4" ?></p>
          <p class="py-2 h5 font-weight-bold">Total: &dollar;<?php echo number_format($pricettl4,2) ?></p>
          <?php if ($product4=="In Stock"){ ?>
            <button type="button" class="btn btn-success w-100"><p class="h5">Order</p></button>
            <?php } else { ?>
              <button type="button" class="btn btn-danger w-100"><p class="h5">N/A</p></button>
          <?php } ?>
        </div>
      </div>

      <!-- Nade card end -->


      <!-- Stefani card start -->

      <div class="card m-3 border rounded card5-bg">
        <img src="<?php echo $img5;?>" class="card-img-top" alt="...">
        <div class="card-body p-4">
          <h3 class="card-title"><?php echo "$name5"; ?> 30% OFF</h3>
          <p class="card-text">This is a watch brand new. A brand new Watch from <?php echo "$name5";?> 
          originals. Waterproof, stainless steel.</p>
          <p class="h5 py-1"> <?php if ($discount5>0){ ?> <del class="text-secondary">US &dollar;<?php echo "$price5" ?> </del> <?php } ?> US &dollar;<?php echo number_format($pricedsc5,2); ?></p>
          <p>Shipping:  &dollar;<?php echo "$shipping5" ?></p>
          <p>Rating:  <?php echo "$rating5" ?></p>
          <p class="py-1 <?php echo "$stockclass" ?>"><?php echo "$product5" ?></p>
          <p class="py-2 h5 font-weight-bold">Total: &dollar;<?php echo number_format($pricettl5,2) ?></p>
          <?php if ($product5=="In Stock"){ ?>
            <button type="button" class="btn btn-success w-100"><p class="h5">Order</p></button>
            <?php } else { ?>
              <button type="button" class="btn btn-danger w-100"><p class="h5">N/A</p></button>
          <?php } ?>
        </div>
      </div>

      <!-- Stefani card end -->


      <!-- Filip card start -->

      <div class="card m-3 border rounded">
        <img src="<?php echo $image6;?>" class="card-img-top" alt="...">
        <div class="card-body p-4" style="background-color:<?php echo $bgColor6;?>;">
          <h3 class="card-title">CAT Tactical <?php echo $discountIndicator6;?></h3>
          <p class="card-text">The new tactical watch from CAT for field use on any terrain and condition.</p>
          <p class="h5"><span style="<?php echo $discountStyle6;?>">US <?php echo $price6;?>$ </span>  <span><?php echo $sellingPriceText6;?></span></p>
          <p>Shipping: $<?php echo $shipping6;?></p>
          <p>Rating:<?php echo $rating6;?></p>
          <p class="<?php echo $stockTextColor6;?>"><?php echo $stockText6;?></p>
          <p class="font-weight-bold">Total: $<?php echo $total6;?></p>
          <button type="button" class="btn  <?php echo $btnColor6;?> w-100 "><p class="h5"><?php echo $btnText6;?></p></button>
        </div>
      </div>

      <!-- Filip card end -->
      
    </div>
      
    </div>
    

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

  </body>
</html>